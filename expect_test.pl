#!/usr/bin/perl

use strict;
use warnings;
use Expect;
use Test::More tests => 3;
use Getopt::Std;

my %opt=();
getopts("fh:i:p:su:x", \%opt) and defined($opt{h}) and defined($opt{u}) or
    die "Usage: [-f] [-s] [-x] [-u <user>] [-p <password>] [-i <id_file>] -h <host>\n";

my $user_host = "$opt{u}\@$opt{h}";

my $exp = Expect->new;
$exp->raw_pty(1);
my $timeout = 20 ;

sub test_ssh_copy_id($) {
  my $command = shift;

  $exp = Expect->spawn($command)
      or die "Cannot spawn ssh-copy-id: $!\n";

  my $spawn_ok = 0;
  my ($matched_pattern_position, $error, $successfully_matching_string, $before_match,
      $after_match) = "";
  ($matched_pattern_position, $error, $successfully_matching_string,
   $before_match, $after_match) =
      $exp->expect($timeout,
                   ['INFO: ', sub {$spawn_ok = 1; exp_continue;}],
                   ['assword: ', sub {my $self = shift; $self->send("$opt{p}\n") ; exp_continue} ],
                   ['This service allows sftp connections only.', sub {note("    found: sftp only")}],
                   ['Number of key\(s\) added:', sub {note("    found: keys added")}],
                   ['WARNING: All keys were skipped', sub {note("    found: All skipped")}],
                   [eof =>
                    sub {
                      if ($spawn_ok) {
                        diag("ERROR: premature EOF in login.");
                      } else {
                        diag("ERROR: could not spawn ssh-copy-id.");
                      }
                    }
                   ],
                   [
                    timeout =>
                    sub {
                      diag("No login.");
                    }
                   ]
      );

  note ("result: ", $matched_pattern_position, " match [", $successfully_matching_string, "] after match [", $after_match, "]") ;
  $exp->soft_close() ;
  return $matched_pattern_position;
}

my $sftp_flag = my $sftp_comment = my $extra_args = '' ;
my $ssh_copy_id = (defined($opt{x}) ? '/bin/sh -x ' : '') . './ssh-copy-id ' ;
if (defined($opt{s})) {
  $sftp_flag = '-s ' ;
  $sftp_comment = '(sftp)';
}

$extra_args .= "-f " if (defined($opt{f})) ;
$extra_args .= "-i $opt{i} " if (defined($opt{i})) ;
# this allows one to pass extra options through to the ssh/sftp calls
$extra_args .= $ARGV[0] if defined($ARGV[0]) ;

 SKIP: {
   skip "cannot provoke failure, as not an SFTP user", 1 unless defined($opt{s});

   is(test_ssh_copy_id($ssh_copy_id . $extra_args . ' ' . $user_host),    3, "get rejected ssh login when SFTP only");
};

is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $user_host), 4, "keys added $sftp_comment");

 SKIP: {
   skip "-f specified, so duplicate key test is meaningless", 1 if defined($opt{f});

   is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $user_host), 5, "All keys skipped $sftp_comment");
}
